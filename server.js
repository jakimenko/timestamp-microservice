const express = require('express');
const strftime = require('strftime');

const app = express();

app.set('port', (process.env.PORT || 3000));

app.get('/:date?', (req, res) => {
  console.log(req.params.date);
  if (!req.params.date) {
    res.send("Hello API world!");
    res.end();
  } else if (parseInt(req.params.date)) {
    obj = {
      unix: req.params.date,
      natural: strftime('%B %d, %Y', new Date(req.params.date * 1000))
    }
    res.json(obj);
    res.end();
  } else {
    const date = Date.parse(req.params.date + 'UTC'); // December 15, 2015
    obj = {
      unix: date / 1000 || null,
      natural: date ? strftime('%B %d, %Y', new Date(date)) : null
    }
    res.json(obj);
    res.end();
  }
});

app.listen(app.get('port'), () => {
  console.log('App is running on port', app.get('port'));
});
